import Vue from 'vue';
import VueRouter from 'vue-router';

import User from '../components/User';
import Footer from '../components/Footer';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        component: User
    },
    {
        path: '/user',
        component: User
    },
    {
        path: '/footer',
        components: Footer
    }
];

const router = new VueRouter( {
    routes
})

export default router;